# README #

This is a simple task management web app with users and tickets. Users are managed with devise. Tickets can be created, read, updated, and deleted. Tickets have titles and descriptions. They also belong to owners and users. No attention has been paid to style. Attention has been paid to tests; however, there are many edge cases and the tests that exist pave the way forward but much paving is needed. One interesting thing about this application is a real-time component that is outlined at the bottom of this page.

### How do I get set up? ###

    brew install postgres
    rvm or rbenv use ruby 2.2.0
    git clone https://_s84@bitbucket.org/_s84/mc-web-server.git
    cd mc-web-server
    bundle install
    rake db:create
    rake db:migrate
    rails server

### Tests ###

    rspec

### How real time works ###

When tickets are created, updated, or deleted clients (browsers such as Chrome or iPhone Safari) that have the tickets index page open will receive real-time updates. Here is an example of how these real-time updates work. Jack opens the website and goes to the tickets index page. Jill does the same thing. At this point Jack and Jill are looking at the same page which has a special client script. This special client script is the sockiet.io client library. This library connects to a [web-socket-server](https://bitbucket.org/_s84/mc-socket-server) which enables bidirectional notifications. Now when Jack updates one of the tickets Jill's page will update in real-time. In the background when Jack updated a ticket a Rails callback posted that ticket to the socket server which has a simple api which accepts post requests. That api takes the post request and then sends it to all the listening web sockets.

![Screen Shot 2016-05-15 at 7.43.22 PM.png](https://bitbucket.org/repo/RkBkjr/images/2302482641-Screen%20Shot%202016-05-15%20at%207.43.22%20PM.png)