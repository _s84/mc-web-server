(function (window) {
  
  var socket = io(window.socketServer);
  var $tickets = $('#tickets');
  
  socket.on('updates', function (data) {
    var action = data.action;
    var ticket = data.ticket;
    switch(action) {
      case 'create':
        return addTicket(ticket);
      case 'update':
        return updateTicket(ticket);
      case 'destroy':
        return removeTicket(ticket);
      default:
        console.error('unknown action', data);
    }
  });
  
  function addTicket(ticket) {
    var html = getTicketOuterHtml(ticket);
    $tickets.prepend(html);
  }
  
  function updateTicket(ticket) {
    var id = getTicketId(ticket);
    var $ticket = $(id);
    var html = getTicketInnerHtml(ticket);
    $ticket.html(html);
  }
  
  function removeTicket(ticket) {
    var id = getTicketId(ticket);
    var $ticket = $(id);
    $ticket.remove();
  }
  
  function getTicketOuterHtml(ticket) {
    var id = getTicketId(ticket);
    var innerHtml = getTicketInnerHtml(ticket);
    return [
      '<tr id="'+id+'">',
      innerHtml,
      '</tr>'
    ].join('');
  }
  
  function getTicketInnerHtml(ticket) {
    var id = ticket.id;
    return [
      '<td>'+ticket.title+'</td>',
      '<td>'+ticket.description+'</td>',
      '<td>'+ticket.owner+'</td>',
      '<td>'+ticket.user+'</td>',
      '<td><a href="/tickets/'+id+'">Show</a></td>',
      '<td><a href="/tickets/'+id+'/edit">Edit</a></td>',
      '<td><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="/tickets/'+id+'">Destroy</a></td>'
    ].join('');
  }
  
  function getTicketId(ticket) {
    return ['#ticket', ticket.id].join('-');
  }

}(window));
