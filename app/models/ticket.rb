class Ticket < ActiveRecord::Base

  ORDER_OPTIONS = [
    ["Created", :created_at],
    ["Updated", :updated_at],
    ["Title", :description],
    ["Owner", :owner_id],
    ["User", :user_id ]
  ]

  @@url = ENV['SOCKET_SERVER'] || 'http://127.0.0.1:3001'

  belongs_to :user
  belongs_to :owner, class_name: User

  validates_presence_of :title
  validates_presence_of :description

  validates :user, presence: true
  validates :owner, presence: true

  after_commit :inform_sockets_create, on: :create
  after_commit :inform_sockets_update, on: :update
  after_commit :inform_sockets_destroy, on: :destroy

  def self.search(params)
    text = params[:text]
    oid = params[:owner_id]
    uid = params[:user_id]
    order = params[:order]
    scope = includes(:user, :owner)
    scope = where(user_id: uid) if uid.present?
    scope = scope.where(owner_id: oid) if oid.present?
    scope = scope.where("title LIKE ? OR description LIKE ?", "%#{text}%", "%#{text}%") if text.present?
    scope = scope.order(order) if order.present?
    scope
  end

  def inform_sockets_create 
    inform_sockets(:create) 
  end

  def inform_sockets_update
    inform_sockets(:update)
  end

  def inform_sockets_destroy
    inform_sockets(:destroy)
  end

  def inform_sockets(action)
    begin
      HTTParty.post(@@url, body: { 
        action: action, ticket: {
          id: self.id,
          title: self.title,
          description: self.description,
          owner: owner.email,
          user: user.email
        }
      }.to_json)
    rescue  Exception => exception
      exception
    end
  end

end
