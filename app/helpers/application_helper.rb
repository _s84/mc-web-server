module ApplicationHelper

  def options_for_user_select(selected)
    options_for_select(User.all.collect {|u| [u.email, u.id] }, selected)
  end

  def socket_io_client
    socket_server + "/socket.io/socket.io.js"
  end

  def socket_server
    @socket_server ||= ENV['SOCKET_SERVER'] || "http://localhost:3001"
  end

end
