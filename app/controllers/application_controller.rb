class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Add an after_sign_up path for staff_user
  
  def after_sign_in_path_for(user)
    tickets_path
  end

  def after_sign_up_path_for(user)
    tickets_path
  end

end
