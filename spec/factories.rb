FactoryGirl.define do

  factory :user do
    email { Faker::Internet.email }
    password { Faker::Internet.password }
  end

  factory :ticket do
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.sentence }
    user { association(:user) }
    owner { association(:user) }
  end

end
