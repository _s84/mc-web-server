require 'spec_helper'

RSpec.describe Ticket, :type => :model do

  describe "Valid Ticket" do
    it "should save" do
      user = FactoryGirl.create(:user)
      ticket = Ticket.create(
        title: Faker::Lorem.sentence, 
        description: Faker::Lorem.sentence, 
        user: user, owner: user
      )
      expect(ticket.errors.messages).to eq({})
    end
  end

  describe "Search" do
    before do
      FactoryGirl.create(:ticket)
      FactoryGirl.create(:ticket)
    end

    it "should find tickets" do
      expect(Ticket.search({}).length).to eq(2)
    end
  end
end
