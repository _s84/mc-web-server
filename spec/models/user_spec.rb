require 'spec_helper'

RSpec.describe User, :type => :model do

  describe "valid user" do
    it "should save" do
      user = User.create(email: Faker::Internet.email, password: Faker::Internet.password)
      expect(user.errors.messages).to eq({})
    end
  end

end
