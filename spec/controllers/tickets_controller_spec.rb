require 'spec_helper'
include Devise::TestHelpers

describe TicketsController do
  before do
    @user = FactoryGirl.create(:user)
  end
  
  before do
    sign_in @user 
  end

  describe 'POST #create' do
    it "should create a ticket" do
      expect {
        post :create, 
        ticket: FactoryGirl.build(:ticket).attributes
      }.to change(Ticket, :count).by(1)
    end
  end

  describe "GET #show" do
    it "should assign the requested contact to @contact" do
      ticket = FactoryGirl.create(:ticket)
      get :show, id: ticket
      assigns(:ticket).should eq(ticket)
    end

    it "should render the #show view" do
      get :show, id: FactoryGirl.create(:ticket)
      response.should render_template :show
    end
  end

  describe "GET #index" do
    it "should populate an array of tickets" do
      ticket = FactoryGirl.create(:ticket)
      get :index
      assigns(:tickets).should eq([ticket])
    end

    it "should render the :index view" do
      get :index
      response.should render_template :index
    end
  end

  describe "GET #show" do
    it "should assign the requested contact to @contact" do
      ticket = FactoryGirl.create(:ticket)
      get :show, id: ticket 
      assigns(:ticket).should eq(ticket)
    end
    
    it "should render the #show view" do
      get :show, id: FactoryGirl.create(:ticket)
      response.should render_template :show
    end
  end 

  describe "GET #new" do
    it "should assign a ticket" do
      get :new
      assigns(:ticket)
    end
  end
end
