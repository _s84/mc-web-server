class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :title
      t.text :description
      t.integer :user_id
      t.integer :owner_id

      t.timestamps null: false
    end
		add_index :tickets, :user_id
		add_index :tickets, :owner_id
  end
end
